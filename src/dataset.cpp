#include <sys/stat.h> // mkdir
#include "dataset.h"

Dataset::Dataset() :
    mDatasetName(""),
    mDir(""),
    mDeferredIo(false),
    mMaxSubDirs(0),
    mIsBinary(false),
    mDelim(","),
    mRootDataTableName("")
{
}

void Dataset::createDataset(std::string datasetName, std::string dir, bool deferredIo, int32_t maxSubDirs, bool isBinary, std::string delim)
{
    mDatasetName = datasetName;
    mDir = dir;
    mDeferredIo = deferredIo;
    mMaxSubDirs = maxSubDirs;

    for (int32_t i = 0; i < mMaxSubDirs; ++i)
    {
        std::string subDir = makeSubDirName(i);
        mkdir((mDir + subDir).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }

    mIsBinary = isBinary;
    if (mIsBinary)
    {
        mDelim = delim;
    }
    else if (delim.size() > 0) // Use single character delimiter for plain text files.
    {
            mDelim = delim[0];
    }
    else
    {
        mDelim = ",";
    }
}

void Dataset::createDataTable(DataRecord *dataRecord)
{
    std::string recordName = dataRecord->getRecordName();

    mRecordInstances[recordName] = dataRecord;

    mDataTables[recordName] =
        std::shared_ptr<DataTable>(new DataTable(this, dataRecord));

    auto &dataTable = mDataTables[recordName];
    /// @note First table created assumed to be root table.
    if (mDataTables.size() == 1)
    {
        dataTable->setRoot(true);
        mRootDataTableName = dataTable->getTableName();
    }
    else
    {
        dataTable->setRoot(false);
    }

    dataTable->init();
}

std::shared_ptr<DataRecord> Dataset::createDataRecord(std::string recordName)
{
    std::shared_ptr<DataRecord> record = mRecordInstances[recordName]->clone();
    return record;
}

std::shared_ptr<DataRecord> Dataset::createDataRecord(DataRecord *dataRecord)
{
    std::string recordName = dataRecord->getRecordName();
    return createDataRecord(recordName);
}

void Dataset::clearDataset()
{
    mDatasetName = "";
    mDir = "";
    mDataTables.clear(); // should call DataTable destructors since smart pointers used.
    mRecordInstances.clear();
    mDeferredIo = false;
    mMaxSubDirs = 0;
    mIsBinary = false;
    mDelim = ",";
    mRootDataTableName = "";
}

void Dataset::initDataset(std::string datasetName, std::string dir, bool deferredIo, int32_t maxSubDirs, bool isBinary, std::string delim)
{
    mDatasetName = datasetName;
    mDir = dir;
    mDeferredIo = deferredIo;
    mMaxSubDirs = maxSubDirs;

    mIsBinary = isBinary;
    if (mIsBinary)
    {
        mDelim = delim;
    }
    else if (delim.size() > 0) // Use single character delimiter for plain text files.
    {
            mDelim = delim[0];
    }
    else
    {
        mDelim = ",";
    }
}

void Dataset::loadDataTable(DataRecord *dataRecord)
{
    std::string recordName = dataRecord->getRecordName();
    mRecordInstances[recordName] = dataRecord;
    mDataTables[recordName] =
        std::shared_ptr<DataTable>(new DataTable(this, dataRecord));

    auto &dataTable = mDataTables[recordName];
    /// @note First table loaded assumed to be root table.
    if (mDataTables.size() == 1)
    {
        dataTable->setRoot(true);
        mRootDataTableName = dataTable->getTableName();
    }
    else
    {
        dataTable->setRoot(false);
    }
    dataTable->load();
}

void Dataset::insertRecord(std::shared_ptr<DataRecord> dataRecord)
{
    std::string recordName = dataRecord->getRecordName();
    auto &dataTable = mDataTables[recordName];
    dataTable->insertRecord(dataRecord);
    if (!mDeferredIo)
    {
        dataTable->append(dataRecord);
        dataTable->markClean(dataRecord); /// @todo move to append?
    }
    else
    {
        dataTable->markDirty(dataRecord);
    }
}

void Dataset::sortTable(std::string tableName)
{
    auto &dataTable = mDataTables[tableName];
    dataTable->sort();
}

void Dataset::saveTable(std::string tableName)
{
    auto &dataTable = mDataTables[tableName];
    dataTable->save();
    dataTable->markClean(); /// @todo move to save?
}

void Dataset::saveTables()
{
    for (auto &mapElement : mDataTables)
    {
        auto &dataTable = mapElement.second;
        if (dataTable->isDirty())
        {
            dataTable->save();
            dataTable->markClean(); /// @todo move to save?
        }
    }
}

std::string Dataset::getDatasetName() const
{
    return mDatasetName;
}

std::string Dataset::getDir() const
{
    return mDir;
}

std::shared_ptr<DataTable> Dataset::getDataTable(std::string tableName) const
{
    return mDataTables.at(tableName);
}

std::list<std::shared_ptr<DataTable>> Dataset::getDataTables() const
{
    std::list<std::shared_ptr<DataTable>> tables;
    for (auto &mapElement : mDataTables)
    {
        tables.push_back(mapElement.second);
    }
    return tables;
}

size_t Dataset::getNumDataTables() const
{
    return mDataTables.size();
}

std::string Dataset::makeSubDirName(int32_t i)
{
    std::string digit = std::to_string(i);
    size_t leadingZeros = digit.size() >= 3 ? digit.size() : 3 - digit.size();
    std::string subDir = std::string(leadingZeros, '0') + digit;
    return subDir;
}

int32_t Dataset::getMaxSubDirs() const
{
    return mMaxSubDirs;
}

std::string Dataset::getRootDataTableName() const
{
    return mRootDataTableName;
}

bool Dataset::getIsBinary() const
{
    return mIsBinary;
}

std::string Dataset::getDelim() const
{
    return mDelim;
}
