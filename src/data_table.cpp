#include <fstream>
#include <algorithm>
#include "dataset.h"
#include "data_record.h"
#include "data_table.h"

DataTable::DataTable(Dataset *dataset, DataRecord *dataRecord) :
    mDataset(dataset),
    mRecordPrototype(dataRecord),
    mIsRoot(false),
    mDirty(false)
{
    mDir = mDataset->getDir();
    mMaxSubDirs = mDataset->getMaxSubDirs();
    mIsBinary = mDataset->getIsBinary();
    mDelim = mDataset->getDelim();
    /// @todo if (!record_prototype_) skip
    mFields = mRecordPrototype->getFields();
    mTableName = mRecordPrototype->getRecordName();
    mVersion = mRecordPrototype->version();
}

std::list<std::string> DataTable::getFields() const
{
    return mFields;
}

std::vector<std::shared_ptr<DataRecord> > DataTable::getRecords() const
{
    return mRecords;
}

void DataTable::insertRecord(std::shared_ptr<DataRecord> record)
{
    if (mMaxSubDirs > 0)
    {
        if (mIsRoot)
        {
            assignTableIndex(record);
        }
    }
    record->setDataTable(mDataset->getDataTable(mTableName));
    mRecords.push_back(record);
}

void DataTable::deleteRecords()
{
    for (auto record : mRecords)
    {
        if (record->shouldDelete() && mIsRoot)
        {
            recoverTableIndex(record);
        }
    }
    mRecords.erase(std::remove_if(mRecords.begin(), mRecords.end(), [](std::shared_ptr<DataRecord> &rec){return rec->shouldDelete();}), mRecords.end());
}

void DataTable::init()
{
    std::ios_base::openmode mode = std::ios_base::out;
    if (mIsBinary)
    {
        mode |= std::ios_base::binary;
    }

    if (mMaxSubDirs == 0)
    {
        mTableFiles.push_back(mDir + mTableName + cExtTable);
        std::ofstream tableOfs(mTableFiles.back(), mode);
        writeTableHeader(tableOfs);
    }
    else // Initialise sub-table in each sub-directory.
    {
        for (int32_t i = 0; i < mMaxSubDirs; ++i)
        {
            std::string subDir = mDataset->makeSubDirName(i);
            mTableFiles.push_back(mDir + subDir + "/" + mTableName + cExtTable);
            std::ofstream tableOfs(mTableFiles.back(), mode);
            writeTableHeader(tableOfs);
        }
    }
}

void DataTable::save()
{
    std::ios_base::openmode mode = std::ios_base::out;
    if (mIsBinary)
    {
        mode |= std::ios_base::binary;
    }

    for (int32_t i : mDirtyIndices)
    {
        /// @todo Use file open mode variable from dataset.
        std::ofstream tableOfs(mTableFiles[static_cast<size_t>(i)], mode);
        writeTableHeader(tableOfs);

        // Write records (to subdirectory if necessary).
        for (auto record : mRecords)
        {
            if (mMaxSubDirs > 0)
            {
                if (mIsRoot)
                {
                    if (static_cast<int32_t>(i) != record->getTableIndex())
                    {
                        continue;
                    }
                }
                else if (static_cast<int32_t>(i) != record->getRootIndex())
                {
                    continue;
                }
                else
                {

                }
            }
            record->serialize(tableOfs, mIsBinary, mDelim);
            tableOfs << std::endl;
        }
    }
}

void DataTable::append(std::shared_ptr<DataRecord> record)
{
    int32_t index = 0;

    if (mMaxSubDirs > 0)
    {
        if (mIsRoot)
        {
            index = record->getTableIndex();
        }
        else
        {
            index = record->getRootIndex();
        }
    }

    std::ios_base::openmode mode = std::ios_base::app;
    if (mIsBinary)
    {
        mode |= std::ios_base::binary;
    }

    std::ofstream tableOfs(mTableFiles[static_cast<size_t>(index)], mode);
    record->serialize(tableOfs, mIsBinary, mDelim);
    tableOfs << std::endl;
}

void DataTable::load()
{
    std::string line;

    // Dataset with no subdirectories.
    if (mMaxSubDirs == 0)
    {
        mTableFiles.push_back(mDir + mTableName + cExtTable);
    }
    else // Dataset spread across subdirectories.
    {
        for (int32_t i = 0; i < mMaxSubDirs; ++i)
        {
            std::string subDir = mDataset->makeSubDirName(i);
            mTableFiles.push_back(mDir + subDir + "/" + mTableName + cExtTable);
        }
    }

    std::ios_base::openmode mode = std::ios_base::in;
    if (mIsBinary)
    {
        mode |= std::ios_base::binary;
    }

    // Store data from subdirectories in single DataTable.
    for (size_t i = 0; i < mTableFiles.size(); ++i)
    {
        std::ifstream tableIfs(mTableFiles[i]);

        /// @todo read and verify version
        //std::getline(tableIfs, line);
        /// @todo read and verify fields
        std::getline(tableIfs, line);

        // Load data records. Root records assigned index based on subdirectory.
        while (tableIfs.good())
        {
            std::shared_ptr<DataRecord> record = mRecordPrototype->clone();
            record->setDataset(mDataset);
            record->deserialize(tableIfs, mIsBinary, mDelim);
            if (!tableIfs.eof())
            {
                if (mMaxSubDirs > 0)
                {
                    if (mIsRoot)
                    {
                        record->setTableIndex(static_cast<int32_t>(i));
                        mTableIndices.erase(static_cast<int32_t>(i));
                    }
                    else
                    {
                        // Set root record.
                        std::list<std::shared_ptr<DataRecord>> selectedRecords;
                        selectedRecords = mDataset->selectRecords<DataRecord, DataRecord::Equal<int32_t>, int32_t>(
                                    mDataset->getRootDataTableName(),
                                    &DataRecord::getTableIndex,
                                    DataRecord::Equal<int32_t>(),
                                    static_cast<int32_t>(i));
                        /// @todo deal with zero or more than one unique key?
                        auto rootRecord = selectedRecords.front();
                        record->setRootRecord(rootRecord);
                    }
                }
                record->setDataTable(mDataset->getDataTable(mTableName));
                mRecords.push_back(record);
            }
        }
    }

    // DataTable is clean after load from file(s).
    mDirtyIndices.clear();
    mDirty = false;
}

void DataTable::clear()
{

}

void DataTable::sort()
{
    std::sort(mRecords.begin(), mRecords.end(), [](std::shared_ptr<DataRecord> lhs, std::shared_ptr<DataRecord> rhs) {
        return *lhs < *rhs;
    });
}

std::string DataTable::getTableName() const
{
    return mTableName;
}

std::vector<std::string> DataTable::getTableFileNames() const
{
    return mTableFiles;
}

bool DataTable::isRoot() const
{
    return mIsRoot;
}

void DataTable::setRoot(bool isRoot)
{
    mIsRoot = isRoot;
    if (mIsRoot)
    {
        for (int32_t i = 0; i < mMaxSubDirs; ++i)
        {
            mTableIndices.insert(i);
        }
    }
}

bool DataTable::isDirty() const
{
    return mDirty;
}

void DataTable::markDirty(std::shared_ptr<DataRecord> record)
{
    mDirty = true;
    if (record)
    {
        if (mMaxSubDirs == 0)
        {
            mDirtyIndices.insert(0);
        }
        else
        {
            int32_t index;
            if (mIsRoot)
            {
                index = record->getTableIndex();
            }
            else
            {
                index = record->getRootIndex();
            }
            if (index != -1)
            {
                mDirtyIndices.insert(index);
            }
            else
            {
                mDirtyIndices.insert(0);
            }
        }
    }
    else // Mark all sub-tables as dirty.
    {
        if (mMaxSubDirs == 0)
        {
            mDirtyIndices.insert(0);
        }
        else
        {
            for (int32_t i = 0; i < mMaxSubDirs; ++i)
            {
                mDirtyIndices.insert(i);
            }
        }
    }
}

void DataTable::markClean(std::shared_ptr<DataRecord> record)
{
    if (record)
    {
        if (mMaxSubDirs == 0)
        {
            mDirtyIndices.clear();
        }
        else
        {
            int32_t index;
            if (mIsRoot)
            {
                index = record->getTableIndex();
            }
            else
            {
                index = record->getRootIndex();
            }
            if (index != -1)
            {
                mDirtyIndices.erase(index);
            }
        }
    }
    else // Mark all sub-tables as clean.
    {
        mDirtyIndices.clear();
    }

    if (mDirtyIndices.empty())
    {
        mDirty = false;
    }
}

std::list<std::shared_ptr<DataRecord> > DataTable::getSubTable(int32_t index) const
{
    std::list<std::shared_ptr<DataRecord>> subTable;
    subTable = mDataset->selectRecords<DataRecord, DataRecord::Equal<int32_t>, int32_t>(
                mTableName,
                &DataRecord::getRootIndex,
                DataRecord::Equal<int32_t>(),
                index);
    return subTable;
}

void DataTable::writeTableHeader(std::ofstream &tableOfs)
{
    //tableOfs << mVersion.first << '.' << mVersion.second << std::endl;

    auto fields = mFields;
    for (auto field = fields.begin(); field != fields.end(); ++field)
    {
        tableOfs << *field;
        if (field != --fields.end())
            tableOfs << mDelim;
    }
    tableOfs << std::endl;
}

int32_t DataTable::assignTableIndex(std::shared_ptr<DataRecord> record)
{
    int32_t index = *mTableIndices.begin();
    mTableIndices.erase(index);
    record->setTableIndex(index);
    return index;
}

void DataTable::recoverTableIndex(std::shared_ptr<DataRecord> record)
{
    int32_t index = record->getTableIndex();
    mTableIndices.insert(index);
}
