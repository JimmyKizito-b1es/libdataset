#include "data_record.h"

DataRecord::DataRecord() :
    mRecordName(""),
    mDataset(nullptr),
    mDataTable(nullptr),
    mRootRecord(nullptr),
    mTableIndex(-1)
{

}

DataRecord::DataRecord(std::string recordName) :
    mRecordName(recordName),
    mDataset(nullptr),
    mDataTable(nullptr),
    mRootRecord(nullptr),
    mTableIndex(-1)
{

}

std::string DataRecord::getRecordName() const
{
    return mRecordName;
}

std::list<std::string> DataRecord::getFields() const
{
    return mFieldNames;
}

void DataRecord::setDataset(Dataset *dataset)
{
    mDataset = dataset;
}

void DataRecord::setDataTable(std::shared_ptr<DataTable> dataTable)
{
    mDataTable = dataTable;
}

std::shared_ptr<DataTable> DataRecord::getDataTable() const
{
    return mDataTable;
}

void DataRecord::setRootRecord(std::shared_ptr<DataRecord> root_record)
{
    mRootRecord = root_record;
}

int32_t DataRecord::getRootIndex() const
{
    int32_t root_index;
    if (mRootRecord)
    {
        root_index = mRootRecord->getTableIndex();
    }
    else
    {
        root_index = -1;
    }
    return root_index;
}

int32_t DataRecord::getTableIndex() const
{
    return mTableIndex;
}

void DataRecord::setTableIndex(int32_t tableIndex)
{
    mTableIndex = tableIndex;
}

bool DataRecord::shouldDelete() const
{
    return mShouldDelete;
}

void DataRecord::setShouldDelete(bool shouldDelete)
{
    mShouldDelete = shouldDelete;
}

bool DataRecord::operator<(const DataRecord &record) const
{
    return mTableIndex < record.mTableIndex;
}
