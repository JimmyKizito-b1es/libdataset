#ifndef DATA_RECORD_H
#define DATA_RECORD_H

#include <memory>
#include <iostream>
#include <map>
#include <list>

class Dataset;
class DataTable;

class DataRecord
{
public:
    DataRecord();
    DataRecord(std::string recordName);
    virtual ~DataRecord() = default;

    virtual std::shared_ptr<DataRecord> clone() = 0;
    virtual std::ostream& serialize(std::ostream& ostream, bool isBinary = false, std::string delim = ",") = 0;
    virtual std::istream& deserialize(std::istream& istream, bool isBinary = false, std::string delim = ",") = 0;
    virtual std::pair<int, int> version() = 0;
    virtual void updateFields(std::shared_ptr<DataRecord> updateRecord) = 0;

    virtual std::string getRecordName() const;
    virtual std::list<std::string> getFields() const;
    virtual void setDataset(Dataset *dataset);
    virtual void setDataTable(std::shared_ptr<DataTable> dataTable);
    virtual std::shared_ptr<DataTable> getDataTable() const;
    virtual void setRootRecord(std::shared_ptr<DataRecord> rootRecord);
    virtual int32_t getRootIndex() const;
    virtual int32_t getTableIndex() const;
    virtual void setTableIndex(int32_t tableIndex);
    virtual bool shouldDelete() const;
    virtual void setShouldDelete(bool shouldDelete);

    virtual bool operator<(const DataRecord& record) const;

    /**
     * Create a single static instance of a record type which will be used
     * as a prototype for cloning non-static records for general use.
     */
    template <class Record>
    static Record* getInstance()
    {
        static Record record;
        return &record;
    }

    template <typename Field>
    struct Less
    {
        bool operator()(const Field &lhs, const Field &rhs) const
        {
            return lhs < rhs;
        }
    };

    template <typename Field>
    struct LessEqual
    {
        bool operator()(const Field &lhs, const Field &rhs) const
        {
            return lhs <= rhs;
        }
    };

    template <typename Field>
    struct Equal
    {
        bool operator()(const Field &lhs, const Field &rhs) const
        {
            return lhs == rhs;
        }
    };

    template <typename Field>
    struct GreaterEqual
    {
        bool operator()(const Field &lhs, const Field &rhs) const
        {
            return lhs >= rhs;
        }
    };

    template <typename Field>
    struct Greater
    {
        bool operator()(const Field &lhs, const Field &rhs) const
        {
            return lhs > rhs;
        }
    };

protected:
    const std::string mRecordName;
    std::list<std::string> mFieldNames;
    Dataset *mDataset;
    std::shared_ptr<DataTable> mDataTable;
    std::shared_ptr<DataRecord> mRootRecord;
    int32_t mTableIndex;
    bool mShouldDelete;

private:
};

#endif // DATA_RECORD_H
