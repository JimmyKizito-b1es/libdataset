#ifndef DATASET_H
#define DATASET_H

#include <string>
#include <list>
#include <memory>
#include <functional>
#include "data_table.h"
#include "data_record.h"

/**
 * Top level Dataset API.
 *
 * Data definition methods control overall structure:
 * - createDataset
 * - createDataTable
 * - createDataRecord
 * - clear*
 * - load*
 *
 * Data manipulation methods control data contents:
 * - insert*
 * - select*
 * - update*
 * - delete*
 */
class Dataset
{
public:
    Dataset();

    /**
     * @brief Initialise Dataset properties and create subdirectories if required.
     * @param datasetName
     * @param dir
     * @param deferredIo
     * @param maxSubDirs
     * @param mode
     * @param delim
     */
    void createDataset(std::string datasetName, std::string dir = "", bool deferredIo = false, int32_t maxSubDirs = 0, bool isBinary = false, std::string delim = ",");

    void createDataTable(DataRecord *dataRecord);

    /** DataRecord factory methods. */
    std::shared_ptr<DataRecord> createDataRecord(std::string recordName);

    std::shared_ptr<DataRecord> createDataRecord(DataRecord *dataRecord);

    /** Set Dataset properties to default values but leave file(s) intact. */
    void clearDataset();

    /**
     * Prepare dataset for loading data tables.
     */
    void initDataset(std::string datasetName, std::string dir = "", bool deferredIo = false, int32_t maxSubDirs = 0, bool isBinary = false, std::string delim = ",");

    /**
     * Load data table from .dt file
     *
     * Register then use static DataRecord instance to deserialize data.
     */
    void loadDataTable(DataRecord *dataRecord);

    /** Add record to data table object and file. */
    void insertRecord(std::shared_ptr<DataRecord> record); /// @note may overload to take raw ptr.

    /**
     * Select records fulfilling search criteria.
     *
     * getField is an accessor method in Record.
     * comp is comparator functor.
     *
     * @returns List of records for which comp(getfield(), field_val) is true.
     */
    template <class Record, class Comparator, class Field>
    std::list<std::shared_ptr<DataRecord>> selectRecords(
            std::string tableName,
            std::function<Field (Record const&)> getField,
            Comparator comp,
            Field field)
    {
        std::list<std::shared_ptr<DataRecord>> records;

        auto &dataTable = mDataTables[tableName];
        auto tableRecords = dataTable->getRecords();

        for (auto record : tableRecords)
        {
            /// @todo check dynamic_cast for nullptr
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), field))
            {
                records.push_back(record);
            }
        }

        return records;
    }

    /** Select from list of records supplied as argument. For compound selection. */
    template <class Record, class Comparator, class Field>
    std::list<std::shared_ptr<DataRecord>> selectRecords(
            std::list<std::shared_ptr<DataRecord>> records,
            std::function<Field (Record const&)> getField,
            Comparator comp,
            Field field)
    {
        std::list<std::shared_ptr<DataRecord>> selectedRecords;

        for (auto record : records)
        {
            /// @todo check dynamic_cast for nullptr
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), field))
            {
                selectedRecords.push_back(record);
            }
        }

        return selectedRecords;
    }

    /**
     * @brief Delete data record.
     *
     * Deletes record from DataTable vector then writes modified DataTable to file.
     *
     * @returns Number of records deleted.
     */
    template <class Record, class Comparator, class Field>
    int deleteRecords(
            std::string tableName,
            std::function<Field (Record const&)> getField,
            Comparator comp,
            Field field)
    {
        int numDel = 0;

        auto &dataTable = mDataTables[tableName];
        auto tableRecords = dataTable->getRecords();

        for (auto record : tableRecords)
        {
            /// @todo check dynamic_cast for nullptr
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), field))
            {
                dataTable->markDirty(record);
                record->setShouldDelete(true);
                ++numDel;
            }
        }

        dataTable->deleteRecords();

        // Optionally defer file I/O.
        if (!mDeferredIo)
        {
            dataTable->save();
            dataTable->markClean(); /// @todo move to save?
        }

        return numDel;
    }

    /// Call after select for compound deletion.
    template <class Record, class Comparator, class Field>
    int deleteRecords(
            std::list<std::shared_ptr<DataRecord>> records,
            std::function<Field (Record const&)> getField,
            Comparator comp,
            Field field)
    {
        int numDel = 0;
        // All records should be in the same table.
        auto table = records.front()->getDataTable();

        for (auto record : records)
        {
            /// @todo check dynamic_cast for nullptr
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), field))
            {
                table->markDirty(record);
                record->setShouldDelete(true);
                ++numDel;
            }
        }

        table->deleteRecords();

        // Optionally defer file I/O.
        if (!mDeferredIo)
        {
            table->save();
            table->markClean(); /// @todo move to save?
        }

        return numDel;
    }

    /// @todo deleteTable(std::string table_name); // Clear all records in table.

    /// @todo deleteTables(); // Clear all records in all tables.

    /** Sort table using DataRecord's overridden '<' operator. */
    void sortTable(std::string tableName);

    /** Save changes made using DataTable's record shared_ptrs to file. */
    void saveTable(std::string tableName);

    /** Write all DataTable objects to files. */
    void saveTables();

    /** Replace matching record(s) with new record. */
    template <class Record, class Comparator, class Field>
    int updateRecords(
            std::string tableName,
            std::function<Field (Record const&)> getField,
            Comparator comp,
            Field field,
            std::shared_ptr<DataRecord> updateRecord)
    {
        int numUpd = 0;

        auto &dataTable = mDataTables[tableName];
        auto tableRecords = dataTable->getRecords();
        for (auto record : tableRecords)
        {
            /// @todo check dynamic_cast for nullptr
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), field))
            {
                dataTable->markDirty(record);
                record->updateFields(updateRecord);
                ++numUpd;
            }
        }

        // Optionally defer file I/O.
        if (!mDeferredIo)
        {
            dataTable->save();
            dataTable->markClean(); /// @todo move to save?
        }

        return numUpd;
    }

    /** Replace matching record(s) with new record. For compound update. */
    template <class Record, class Comparator, class Field>
    int updateRecords(
            std::list<std::shared_ptr<DataRecord>> records,
            std::function<Field (Record const&)> getField,
            Comparator comp,
            Field field,
            std::shared_ptr<DataRecord> updateRecord)
    {
        int numUpd = 0;
        // All records should be in the same table.
        auto table = records.front()->getDataTable();

        for (auto record : records)
        {
            /// @todo check dynamic_cast for nullptr
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), field))
            {
                table->markDirty(record);
                record->updateFields(updateRecord);
                ++numUpd;
            }
        }

        // Optionally defer file I/O.
        if (!mDeferredIo)
        {
            table->save();
            table->markClean(); /// @todo move to save?
        }

        return numUpd;
    }

    /** Change single field in record(s). */
    template <class Record, class Comparator, class FieldSelect, class FieldUpdate>
    int updateField(
            std::string tableName,
            std::function<FieldSelect (Record const&)> getField,
            Comparator comp,
            FieldSelect fieldSel,
            std::function<void (Record &, FieldUpdate)> setField,
            FieldUpdate fieldUpd)
    {
        int numUpd = 0;

        auto &table = mDataTables[tableName];
        auto tableRecords = table->getRecords();

        for (auto record : tableRecords)
        {
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), fieldSel))
            {
                table->markDirty(record);
                setField(rec, fieldUpd);
                table->markDirty(record); /// @todo Is this needed in case record moved to new sub-table by update?
                ++numUpd;
            }
        }

        // Optionally defer file I/O
        if (!mDeferredIo)
        {
            table->save();
            table->markClean(); /// @todo move to save?
        }

        return numUpd;
    }

    /** Change single field in record(s). Call after select for compound update. */
    template <class Record, class Comparator, class FieldSelect, class FieldUpdate>
    int updateField(
            std::list<std::shared_ptr<DataRecord>> records,
            std::function<FieldSelect (Record const&)> getField,
            Comparator comp,
            FieldSelect fieldSel,
            std::function<void (Record &, FieldUpdate)> setField,
            FieldUpdate fieldUpd)
    {
        int numUpd = 0;

        auto table = records.front()->getDataTable();

        for (auto record : records)
        {
            Record &rec = *std::dynamic_pointer_cast<Record>(record);
            if (comp(getField(rec), fieldSel))
            {
                table->markDirty(record);
                setField(rec, fieldUpd);
                table->markDirty(record); /// @todo Is this needed in case record moved to new sub-table by update?
                ++numUpd;
            }
        }

        // Optionally defer file I/O
        if (!mDeferredIo)
        {
            table->save();
            table->markClean(); /// @todo move to save?
        }

        return numUpd;
    }

    std::string getDatasetName() const;
    std::string getDir() const;
    std::shared_ptr<DataTable> getDataTable(std::string table_name) const;
    std::list<std::shared_ptr<DataTable>> getDataTables() const;
    size_t getNumDataTables() const;
    std::string makeSubDirName(int32_t i);
    int32_t getMaxSubDirs() const;
    std::string getRootDataTableName() const;
    bool getIsBinary() const;
    std::string getDelim() const;

private:
    std::string mDatasetName;
    std::string mDir;

    std::map<std::string, DataRecord*> mRecordInstances;
    std::map<std::string, std::shared_ptr<DataTable>> mDataTables;

    bool mDeferredIo;
    int32_t mMaxSubDirs;
    bool mIsBinary;
    std::string mDelim;
    std::string mRootDataTableName;
};

#endif // DATASET_H
