#ifndef DATA_TABLE_H
#define DATA_TABLE_H

#include <string>
#include <list>
#include <vector>
#include <memory>
#include <set>

class Dataset;
class DataRecord;

class DataTable
{
public:
    DataTable(Dataset *dataset, DataRecord *dataRecord);

    std::list<std::string> getFields() const;

    std::vector<std::shared_ptr<DataRecord>> getRecords() const;

    /**
     * Insert new data record into table.
     *
     * For root table, also assign table index to record.
     */
    void insertRecord(std::shared_ptr<DataRecord> record);

    /**
     * @brief Remove records marked for deletion.
     */
    void deleteRecords();

    /** Initialise new data table file. */
    void init();

    /** Save records to file. Overwrites */
    void save();

    void append(std::shared_ptr<DataRecord> record);

    /** Load records from file. */
    void load();

    /** Clear - currently default destructor is enough. */
    void clear();

    /** Sort table using DataRecord's overridden '<' operator. */
    void sort();

    std::string getTableName() const;
    std::vector<std::string> getTableFileNames() const;
    bool isRoot() const;
    void setRoot(bool isRoot);
    bool isDirty() const;
    void markDirty(std::shared_ptr<DataRecord> record = nullptr);
    void markClean(std::shared_ptr<DataRecord> record = nullptr);
    std::list<std::shared_ptr<DataRecord>> getSubTable(int32_t index) const;

    /// @todo setFileMode(); // text | bin

private:
    /** Write header: version then column headings. */
    void writeTableHeader(std::ofstream &table_ofs);

    /** Assign table indices by removing elements from mTableIndices set without replacement. */
    int32_t assignTableIndex(std::shared_ptr<DataRecord> record);

    /** Return table index to pool of available indices mTableIndices. */
    void recoverTableIndex(std::shared_ptr<DataRecord> record);

    Dataset *mDataset;
    /** Pointer to statically allocated record prototype instance. */
    DataRecord *mRecordPrototype;
    std::string mTableName;
    std::vector<std::string> mTableFiles;
    std::string mDir;
    std::list<std::string> mFields;
    std::pair<int, int> mVersion;
    std::vector<std::shared_ptr<DataRecord>> mRecords;
    bool mIsRoot;
    int32_t mMaxSubDirs;
    std::set<int32_t> mTableIndices;
    bool mDirty;
    std::set<int32_t> mDirtyIndices;
    bool mIsBinary;
    std::string mDelim;

    const std::string cExtTable = ".dt";
};

#endif // DATA_TABLE_H
